package zzz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class Median {
	private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	private static List<Integer> originalList = new LinkedList<>();
	private static List<Integer> list = new LinkedList<>();

	public static void main(String[] args) throws NumberFormatException, IOException {
		insertNum();
	}

	public static void insertNum() throws NumberFormatException, IOException {
		System.out.println("Enter number (-1 to exit): ");
		int n = Integer.parseInt(reader.readLine());
		list.add(getIndex(n), n);
		originalList.add(n);
		if (n == -1) {
			System.exit(0);
		}
		printMedian();
	}

	private static int getIndex(int n) {
		if (list.size() == 0)
			return 0;
		if (list.size() == 1) {
			return list.get(0) > n ? 0 : 1;
		}
		return binarySearchLocation(n);
	}

	private static int binarySearchLocation(int n) {
		int start = 0, end = list.size() - 1, mid = 0;
		while (start <= end) {
			mid = (int) Math.floor((start + end) / 2.0);
			if (list.get(mid) < n) {
				start = mid + 1;
			} else if (list.get(mid) > n) {
				end = mid - 1;
			} else {
				break;
			}
		}
		return list.get(mid) >= n ? mid : mid+1;
	}

	public static void printMedian() throws NumberFormatException, IOException {
		double median = 0;
		if (list.size() % 2 == 0) {
			median = (list.get(list.size() / 2 - 1) + list.get(list.size() / 2)) / 2.0;
		} else {
			median = list.get(list.size() / 2);
		}
		System.out.println("median of the numbers " + originalList + "  is : " + median);
		insertNum();
	}
}
